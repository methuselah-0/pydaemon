# Pydaemon - Use Python from GNU Bash.
# Copyright © 2022 David Larsson <david.larsson@selfhosted.xyz>
#
# This file is part of pydaemon.
#
# Pydaemon is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Pydaemon is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pydaemon.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import signal
import sys
import socket
import os
import traceback
import types

from threading import Thread

from io import StringIO
import contextlib

_HOST = str(sys.argv[1])
_PORT = int(sys.argv[2])
_UNACCEPTED_COUNT = 10
_MAX_BUFFER_SIZE = 4096

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

def client_thread(conn, ip, port):
    # Receive input, check size
    input_from_client_bytes = conn.recv(_MAX_BUFFER_SIZE)
    #input_from_client_bytes = conn.recv()
    #size = sys.getsizeof(input_from_client_bytes)

    # Decode and strip
    input_from_client = input_from_client_bytes.decode("utf8").rstrip()

    # Perform action
    #res = eval(input_from_client)
    # Encode result, send message, and close the connection
    #vsysl = res.encode("utf8")
    #conn.sendall(vsys1)
    #conn.close()

    with stdoutIO() as s:
        try:
            #exec(input_from_client)
            exec(input_from_client, globals())
        except:
            eprint("Something wrong with the code")

    #vsys1 = s.encode("utf8")
    #print("out:", s.getvalue())
    s2 = str(s.getvalue())
    s3 = s2.encode("utf8")
    conn.sendall(s3)
    #conn.sendall(vsys1)
    conn.close()

def start_server():
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this is for easy starting/killing the app
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    try:
        soc.bind((_HOST, _PORT))

    except socket.error as msg:
        sys.exit()

    # Start listening on socket
    soc.listen(_UNACCEPTED_COUNT)

    # Infinite loop while script is "active"
    try:
        while True:
            conn, addr = soc.accept()
            ip, port = str(addr[0]), str(addr[1])
            try:
                Thread(target=client_thread, args=(conn, ip, port)).start()
            except Exception as msg:
                traceback.print_exc()
    except (KeyboardInterrupt, SystemExit):
        soc.close()
        
    soc.close()

def signal_term_handler(signal, frame):
    print('got SIGTERM. Exiting..')
    sys.exit(0)
 
signal.signal(signal.SIGTERM, signal_term_handler)

start_server()
