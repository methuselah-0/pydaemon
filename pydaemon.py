# Pydaemon - Use Python from GNU Bash.
# Copyright © 2022 David Larsson <david.larsson@selfhosted.xyz>
#
# This file is part of pydaemon.
#
# Pydaemon is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Pydaemon is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pydaemon.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import signal
import sys
import socket
import os
import traceback
import types

from threading import Thread

from io import StringIO
import contextlib

_SOCK_DIR = "/tmp/pydaemon/"
_SOCK_END = ".sock"
_SOCK_NAME = str(sys.argv[1])

_SOCK_PATH = _SOCK_DIR + _SOCK_NAME + _SOCK_END

_PID_END = ".pid"
_SOCK_PID = _SOCK_PATH + _PID_END

_MAX_BUFFER_SIZE = 4096

# print(_SOCK_DIR)
# print(_SOCK_END)
# print(_SOCK_NAME)
# print(_SOCK_PATH)
# print(_PID_END)
# print(_SOCK_PID)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

def client_thread(conn):
    # Receive input, check size
    input_from_client_bytes = conn.recv(_MAX_BUFFER_SIZE)
    #input_from_client_bytes = conn.recv()

    # Decode and strip
    input_from_client = input_from_client_bytes.decode("utf8").rstrip()

    # Perform action
    #res = eval(input_from_client)
    # Encode result, send message, and close the connection
    #vsysl = res.encode("utf8")
    #conn.sendall(vsys1)
    #conn.close()

    with stdoutIO() as pydaemon_s:
        try:
            exec(input_from_client, globals())
        #except:
        #    eprint("Something wrong with the code")
        except Exception as msg:
            traceback.print_exc()
            print(msg)

    #s2 = str(s.getvalue())
    #s3 = s2.encode("utf8")
    conn.sendall(str(pydaemon_s.getvalue()).encode("utf8"))
    conn.close()

def start_server(_SOCK_PATH, _SOCK_PID):
    server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    try:
        if os.path.exists(_SOCK_PATH):
            os.remove(_SOCK_PATH)
        if os.path.exists(_SOCK_PID):
            os.remove(_SOCK_PID)
        server.bind(_SOCK_PATH)
    except socket.error as msg:
        traceback.print_exc()        
        sys.exit()

    # Start listening on socket
    server.listen(1)

    try:
        with open(_SOCK_PID, "w") as pid_file:
            print("{}".format(os.getpid()), file=pid_file)
    except socket.error as msg:
        traceback.print_exc()        
        sys.exit()
    
    # Infinite loop while script is "active"
    try:
        while True:
            conn,_ = server.accept()
            try:
                Thread(target=client_thread, args=(conn,)).start()
            except Exception as msg:
                traceback.print_exc()
    except (KeyboardInterrupt, SystemExit):
        server.close()
        if os.path.exists(_SOCK_PATH):
            os.remove(_SOCK_PATH)
        if os.path.exists(_SOCK_PID):
            os.remove(_SOCK_PID)
        
    server.close()

def signal_term_handler(signal, frame):
    eprint('pydaemon: received SIGTERM. Exiting..')
    if os.path.exists(_SOCK_PATH):
        os.remove(_SOCK_PATH)
    if os.path.exists(_SOCK_PID):
        os.remove(_SOCK_PID)
        
    sys.exit(0)
 
signal.signal(signal.SIGTERM, signal_term_handler)

start_server(_SOCK_PATH, _SOCK_PID)
