#!/usr/bin/env bash
# Pydaemon - Use Python from GNU Bash.
# Copyright © 2022 David Larsson <david.larsson@selfhosted.xyz>
#
# This file is part of pydaemon.
#
# Pydaemon is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Pydaemon is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pydaemon.  If not, see <http://www.gnu.org/licenses/>.

pydaemon(){
    mkdir -p /tmp/pydaemon && {
	if [[ -n "$_PYDAEMON_GROUP" ]]; then
	    chown "$USER":"$_PYDAEMON_GROUP" /tmp/pydaemon
	else
	    chown "$USER":$(id -gn) /tmp/pydaemon; fi
    }
    local pid channel
    channel="${1:-pydaemon}"
    [[ -e /tmp/pydaemon/"$channel".sock.pid ]] && {
	pid=$(cat /tmp/pydaemon/"$channel".sock.pid)
	kill "$pid"
    }

local a
local b
a="${2:-127.0.0.1}"
b="${3:-55023}"

export _PYDAEMON_SOCK_TYPE

if [[ -n "$2" ]] && [[ -n "$3" ]]; then
    cat <<EOF>/tmp/pydaemon.conf
_PYDAEMON_HOST=$a
_PYDAEMON_PORT=$b
_PYDAEMON_SOCK_TYPE=TCP_STREAM
EOF
    export _PYDAEMON_HOST
    export _PYDAEMON_PORT

    python3 "$(dirname "${BASH_SOURCE[0]}")"/py-net-daemon.py "$a" "$b" &
else
    cat <<EOF>/tmp/pydaemon.conf
_PYDAEMON_SOCK_TYPE=UNIX_STREAM
EOF

    python3 "$(dirname "${BASH_SOURCE[0]}")"/pydaemon.py "${channel}" &
fi

source /tmp/pydaemon.conf

}
[[ ! "${1}" == '--source-only' ]] && pydaemon "$@"

pypipe(){
    local channel
    [[ $# -gt 1 ]] && channel="$1" && shift 1

    channel="${channel:-pydaemon}"

    [[ ! -e /tmp/pydaemon/"$channel".sock.pid ]] && {
	. "${BASH_SOURCE[0]}" "$channel"
	while [[ ! -e /tmp/pydaemon/"$channel".sock.pid ]]; do
	    sleep 0.1; done
    }
    if [[ "$_PYDAEMON_SOCK_TYPE" == UNIX_STREAM ]]; then
	(
	flock 206
	printf '%b' "$1" $'\n' | socat - UNIX-CLIENT:/tmp/pydaemon/"$channel".sock
	) 206>/tmp/pydaemon_unix_sock_lock
    elif [[ "$_PYDAEMON_SOCK_TYPE" == TCP_STREAM ]]; then
	(
	flock 206
	printf '%b' "$1" $'\n' | nc "$_PYDAEMON_HOST" "$_PYDAEMON_PORT"
	) 206>/tmp/pydaemon_nc_lock
    fi
}
# pypipe(){
#     [[ ! -e /tmp/pydaemon.pid ]] && {
# 	. "${BASH_SOURCE[0]}"
# 	while [[ ! -e /tmp/pydaemon.sock ]]; do
# 	    sleep 0.1; done
#     }
#     if [[ "$_PYDAEMON_SOCK_TYPE" == UNIX_STREAM ]]; then
# 	printf '%b' "$1" | socat - UNIX-CLIENT:/tmp/pydaemon.sock
#     elif [[ "$_PYDAEMON_SOCK_TYPE" == TCP_STREAM ]]; then
# 	printf '%b' "$1" | nc "$_PYDAEMON_HOST" "$_PYDAEMON_PORT"
#     fi
# }
